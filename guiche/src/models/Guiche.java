package models;

public class Guiche {
	int id = 0;
	Listas.LinkedList<String> senhaP = new Listas.LinkedList<>();
	Listas.LinkedList<String> senhaC = new Listas.LinkedList<>();
	Listas.LinkedList<String> senhaJ = new Listas.LinkedList<>();
	
	
	//getters
	public Listas.LinkedList<String> getSenhaP() {
		return senhaP;
	}
	
	public int getId() {
		return id;
	}

	public void setSenhaC(Listas.LinkedList<String> senhaC) {
		this.senhaC = senhaC;
	}
	
	public void setSenhaJ(Listas.LinkedList<String> senhaJ) {
		this.senhaJ = senhaJ;
	}

	//setters
	public void setId(int id) {
		this.id = id;
	}
	
	public void setSenhaP(Listas.LinkedList<String> senhaP) {
		this.senhaP = senhaP;
	}

	public Listas.LinkedList<String> getSenhaC() {
		return senhaC;
	}
	
	Listas.LinkedList<String> getSenhaJ() {
		return senhaJ;
	}
	

	//metodo principal
	public Guiche(int id, Listas.LinkedList<String> senhaP, Listas.LinkedList<String> senhaC, Listas.LinkedList<String> senhaJ)
	{
		setId(id);
		setSenhaC(senhaC);
		setSenhaJ(senhaJ);
		setSenhaP(senhaP);
	}

	public Guiche(Integer valueOf) {
		id = valueOf;
	}
	
}
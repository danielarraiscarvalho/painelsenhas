package Crontrollers;

import Views.PainelGuiche;
import models.Senha;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class CtrlGuiche implements ActionListener {
	PainelGuiche painelGuiche;

	// contrutor do controlador
	public CtrlGuiche(PainelGuiche tela) {
		painelGuiche = tela;
	}

	@Override
	// sobrecrita do metodo actionPerformed da classe actionlinestter
	public void actionPerformed(ActionEvent e) {
		// verificar se o butao pressionado foi o de proxima senha
		if (e.getSource() == painelGuiche.getBtnProxima()) {
			// verifica se ha senhas no painel
			if (painelGuiche.totalSenha > 0) {
				try {
					// captura a senha e a remove do painel
					Object o = painelGuiche.proximaSenha();
					Senha senha = (Senha) o;
					// verifica se a senha que veio nao e nulla, se nao for ela
					// envia a senha juntamente com cabecalho CHAMAR
					if (senha != null) {
						painelGuiche.canalSaida.writeUTF("CHAMAR:" + painelGuiche.idGuiche + ";" + senha.getCategoria()
								+ ";" + senha.getSenha());
						painelGuiche.canalSaida.flush();
					}
				} catch (IOException e1) {
				}
			} else {
				// caso nao haja ele mostra ao usurario
				painelGuiche.getLblProximasSenhas().setText("Sem senhas!!!");
			}

		}
	}

}

create table guiches
(
  id_guiche serial                not null,
  aberto    boolean default false not null,
  constraint table_name_pkey
  primary key (id_guiche)
);

alter table guiches
  owner to postgres;

create table senhas_retiradas
(
  senha     serial                not null,
  categoria varchar(1)            not null,
  chamada   boolean default false not null,
  guiche    integer,
  horario   timestamp(1) default NULL :: timestamp without time zone,
  horario_r timestamp(1) default NULL :: timestamp without time zone,
  constraint senhas_retiradas_pkey
  primary key (senha),
  constraint fk_guiche
  foreign key (guiche) references guiches
);

alter table senhas_retiradas
  owner to postgres;


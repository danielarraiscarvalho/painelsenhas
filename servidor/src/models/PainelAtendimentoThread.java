package models;

import views.Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class PainelAtendimentoThread extends Thread{
	    //Atributos da classe
	    Socket cliente = null;
	    DataInputStream entrada = null;
	    DataOutputStream saida = null;
	    Servidor copiaServidor = null;
	    
	    //Construtor da classe
	    public PainelAtendimentoThread(Socket cliente, Servidor copia)
	    {
	    	copiaServidor = copia;
	        this.cliente = cliente;
	        try
	        {
	            this.entrada = new DataInputStream(cliente.getInputStream());
	            this.saida = new DataOutputStream(cliente.getOutputStream());
	            this.start();
	        }
	        catch (Exception e) { }
	    }
	    
	    public void enviaMensagem(String msg)
	    {
	        try
	        {
	            saida.writeUTF(msg);
	            saida.flush();
	        }
	        catch (Exception e) { }
	    }
	    
	    //Define o comportamento da Thread
	    public void run()
	    {
	        while(true)
	        {
	        		Senha s = null;
	            try
	            {
	                String novaMensagem = entrada.readUTF();
	                String[] dados = novaMensagem.split(":");
	                if (novaMensagem != null) {
	    				switch (dados[0]) {
	    				case "NOVASENHA":
	    					s = copiaServidor.crud.novaSenha(dados[1]);
	    					break;
	    				default:
	    					break;
	    				}
	    			}
	                enviaMensagem("SENHA:"+s.categoria+";"+s.senha);
	            }
	            catch (Exception e) { }
	        }
	    }
	}
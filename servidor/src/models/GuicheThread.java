package models;

import views.Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class GuicheThread extends Thread {
	// Atributos da classe
	public Socket cliente = null;
	public DataInputStream entrada = null;
	DataOutputStream saida = null;
	Servidor copiaServidor = null;

	String id_thread = null;
	Guiche g = null;

	// Construtor da classe
	public GuicheThread(Socket cliente, Servidor copia) {
		copiaServidor = copia;
		this.cliente = cliente;
		try {
			this.entrada = new DataInputStream(cliente.getInputStream());
			this.saida = new DataOutputStream(cliente.getOutputStream());
			this.start();
		} catch (Exception e) {
		}
	}

	// envia mensagem
	public void enviaMensagem(String msg) {
		try {
			saida.writeUTF(msg);
			saida.flush();
		} catch (Exception e) {
		}
	}

	// Define o comportamento da Thread
	public void run() {
		while (true) {
			String msg = null;
			try {
				msg = entrada.readUTF();
			} catch (IOException e) {
			}

			// verifica se a mensagem n�o � nula
			if (msg != null) {

				// separa cabe�alho da mensagem dos dados
				String[] cabecalho = msg.split(":");
				if (cabecalho.length > 1) {

					String[] dados = cabecalho[1].split(";");

					g = new Guiche(Integer.valueOf(dados[0]));

					switch (cabecalho[0]) {
					// verificao conteudo do cabe�alho e executar algo de acordo com cabe�alho recebido
					case "IDGUICHE":
						try {
							copiaServidor.crud.cadastrarGuiche(g);
							copiaServidor.crud.abrirGuiche(g);
						} catch (Exception e) {
						}
						break;
					case "CHAMAR":
						copiaServidor.crud.chamarSenha(new Senha(dados[1], Integer.valueOf(dados[2])), g);
						copiaServidor.atualizaOrdem();
						if (copiaServidor.proximaCategoria.equals(dados[1])) {
							copiaServidor.atualizaOrdem();
						}
						break;
					default:
						break;
					}
				}
			}

		}
	}
}
